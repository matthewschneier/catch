from random import randint

import pygame
from pygame.sprite import Sprite

class Ball(Sprite):
    """A class to the ball."""

    def __init__(self,ai_settings,screen):
        super().__init__()
        self.screen = screen
        x = randint(0,1200)
        height = 800

        #Create ball at random position.
        self.rect = pygame.Rect(x,0,ai_settings.ball_width,ai_settings.ball_height)

        #Store ball's position as decimal.
        self.y = float(self.rect.y)
        self.color = ai_settings.ball_color
        self.speed_factor = ai_settings.ball_speed_factor

    def update(self):
        """Move the ball down."""
        #Update position of ball.
        self.y += self.speed_factor
        #Update rect position.
        self.rect.y = self.y

    def draw_ball(self):
        """Draw the ball."""
        pygame.draw.rect(self.screen,self.color,self.rect)
