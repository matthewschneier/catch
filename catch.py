import pygame
from pygame.sprite import Group

from settings import Settings
from man import Man
from stats import Stats
from button import Button
from scoreboard import Scoreboard
import gameFunctions as gf

def run_game():
    """Initialize game and create a screen object."""
    pygame.init()
    ai_settings = Settings()
    screen = pygame.display.set_mode((
        ai_settings.screen_width, ai_settings.screen_height))

    #Box caption.
    pygame.display.set_caption('Catch')

    #Make the Play button.
    play_button = Button(ai_settings,screen,'Play')

    #Create instance to store game stats and scoreboard.
    stats = Stats(ai_settings)
    sb = Scoreboard(ai_settings,screen,stats)

    #Set background color.
    bg_color = (175,175,175)

    #Make man.
    man = Man(ai_settings,screen)

    #Make ball.
    balls = Group()

    #Start main loop of game.
    while True:
        gf.check_events(ai_settings,screen,stats,sb,play_button,man,balls)
        if stats.game_active:
            man.update()
            gf.update_balls(ai_settings,screen,stats,sb,man,balls)
        gf.update_screen(ai_settings,screen,stats,sb,man,balls,play_button)

run_game()
