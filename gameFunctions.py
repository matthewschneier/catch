import sys
from time import sleep
from random import randint

import pygame
from pygame.sprite import Group
import json

from ball import Ball
from man import Man

def check_events(ai_settings,screen,stats,sb,play_button,man,balls):
    """Respond to keypresses and mouse events."""
    for event in pygame.event.get():

        #Quitting game.
        if event.type == pygame.QUIT:
            sys.exit()

        #Ship movement.
        elif event.type == pygame.KEYDOWN:
            mouse_x,mouse_y = pygame.mouse.get_pos()
            check_keydown_events(
                ai_settings,screen,event,stats,sb,play_button,man,balls,mouse_x,mouse_y)

        elif event.type == pygame.KEYUP:
            check_keyup_events(event,man)

        #Play button
        elif event.type == pygame.MOUSEBUTTONDOWN:
            mouse_x,mouse_y = pygame.mouse.get_pos()
            check_play_button(
                ai_settings,screen,stats,sb,play_button,man,balls,mouse_x,mouse_y)

def check_play_button(
    ai_settings,screen,stats,sb,play_button,man,balls,mouse_x,mouse_y):
    """Call start game if play button clicked."""
    button_clicked = play_button.rect.collidepoint(mouse_x,mouse_y)
    if button_clicked and not stats.game_active:
        start_game(
            ai_settings,screen,stats,sb,play_button,man,balls,mouse_x,mouse_y)

def start_game(
    ai_settings,screen,stats,sb,play_button,man,balls,mouse_x,mouse_y):
    """Start a new game when player initiates play."""
    #Reset game settings.
    ai_settings.initialize_dynamic_settings()

    #Reset game stats.
    stats.reset_stats()

    #Reset the scoreboard images.
    sb.prep_score()
    sb.prep_high_score()
    sb.prep_level()
    sb.prep_men()

    #Empty list of balls.
    balls.empty()

    #Create new ball.
    create_ball(ai_settings,screen,man,balls)

    #Hide mouse.
    pygame.mouse.set_visible(False)
    if play_button.rect.collidepoint(mouse_x,mouse_y):
        stats.game_active = True

def check_keydown_events(
    ai_settings,screen,event,stats,sb,play_button,man,balls,mouse_x,mouse_y):
    """Respond to keypresses."""
    if event.key == pygame.K_RIGHT:
        man.moving_right = True
    elif event.key == pygame.K_LEFT:
        man.moving_left = True
    elif event.key == pygame.K_SPACE:
        drop_ball(ai_settings,screen,man,balls)
    elif event.key == pygame.K_p:
        if not stats.game_active:
            start_game(
                ai_settings,screen,stats,sb,play_button,man,balls,mouse_x,mouse_y)
    elif event.key == pygame.K_q:
        sys.exit()

def check_keyup_events(event,man):
    """Respond to key releases."""
    if event.key == pygame.K_RIGHT:
        man.moving_right = False
    elif event.key == pygame.K_LEFT:
        man.moving_left = False

def drop_ball(ai_settings,screen,man,balls):
    """Fire a bullet if limit not reached yet."""
    #Create new bullet and add it to the bullets group.
    if len(balls) < ai_settings.balls_allowed:
        new_ball = Ball(ai_settings,screen)
        balls.add(new_ball)
        create_ball(ai_settings,screen,man,balls)

def update_balls(ai_settings,screen,stats,sb,man,balls):
    """Update position of balls."""
    #Update ball position.
    balls.update()

    check_balls_bottom(ai_settings,screen,stats,sb,man,balls)

    check_ball_man_collisions(ai_settings,screen,stats,sb,man,balls)

def check_ball_man_collisions(ai_settings,screen,stats,sb,man,balls):
    """Respond to man and ball collisions."""
    #Remove ball when hits man.
    man = Group(man)
    collisions = pygame.sprite.groupcollide(man,balls,False,True)

    #Increase score.
    if collisions:
        for balls in collisions.values():
            stats.score += ai_settings.ball_points * len(balls)
            sb.prep_score()
        check_high_score(stats,sb)

    #New ball
    if len(balls) == 0:
        balls.empty()
        ai_settings.increase_speed()
        stats.level += 1
        sb.prep_level()
        create_ball(ai_settings,screen,man,balls)

def check_balls_bottom(ai_settings,screen,stats,sb,man,balls):
    """Check if any balls have hit bottom."""
    screen_rect = screen.get_rect()
    for ball in balls.sprites():
        if ball.rect.bottom >= screen_rect.bottom:
            lose_life(ai_settings,screen,stats,sb,man,balls)
            break

def lose_life(ai_settings,screen,stats,sb,man,balls):
    """Respond to dropping ball."""
    #Decrement lives
    if stats.lives_left > 0:
        stats.lives_left -= 1

        #Update scoreboard.
        sb.prep_men()

        #Empty balls.
        balls.empty()

        #Create new ball and center man
        create_ball(ai_settings,screen,man,balls)
        man.center_man()

        #Pause
        sleep(0.5)
    else:
        stats.game_active = False
        pygame.mouse.set_visible(True)

def update_screen(ai_settings,screen,stats,sb,man,balls,play_button):
    """Update images on screen and flip to new screen."""
    #Redraw screen.
    screen.fill(ai_settings.bg_color)

    #Redraw ball
    for ball in balls.sprites():
        ball.draw_ball()
    man.blitme()

    #Draw scoreboard
    sb.show_score()

    #Draw play button if game is inactive
    if not stats.game_active:
        play_button.draw_button()

    #Make the most recently draw screen visible.
    pygame.display.flip()

def create_ball(ai_settings,screen,man,balls):
    """Fire ball if limit not reached."""
    #Create new bullet and add to balls.
    if len(balls) < ai_settings.balls_allowed:
        new_ball = Ball(ai_settings,screen)
        balls.add(new_ball)

def check_high_score(stats,sb):
    """Check to see if there's a new high score."""
    filename = 'highscore.json'
    with open(filename,'r+') as f:
        if stats.score > stats.high_score:
            stats.high_score = stats.score
            json.dump(stats.high_score,f)
            sb.prep_high_score()
