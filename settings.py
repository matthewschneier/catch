class Settings():
    """A class to store all settings for Cathcer."""

    def __init__(self):
        """Initialize game's settings."""
        #Screen settings.
        self.screen_width = 1200
        self.screen_height = 800
        self.bg_color = (175,175,175)

        #Fail settings.
        self.lives_left = 3
        self.lives_limit = 3

        #Ball settings.
        self.ball_width = 30
        self.ball_height = 30
        self.ball_color = (200,10,10)
        self.balls_allowed = 2

        #Level up settings.
        self.speedup_scale = 1.05
        self.score_scale = 1.5
        self.initialize_dynamic_settings()

    def initialize_dynamic_settings(self):
        """Initialize settings that change throughout the game."""
        self.man_speed_factor = 3
        self.ball_speed_factor = 1.5

        #Scoring.
        self.ball_points = 10

    def increase_speed(self):
        """Increase speed and point settings."""
        self.ball_speed_factor *= self.speedup_scale
        self.man_speed_factor *= self.speedup_scale
        self.ball_points = int(self.ball_points * self.score_scale)
