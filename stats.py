class Stats():
    """Track statistics for Alien Invasion."""

    def __init__(self,ai_settings):
        """Initialize settings."""
        self.ai_settings = ai_settings
        self.reset_stats()

        #Start Alien Invasion in an inactive state.
        self.game_active = False

        #High score should never be reset.
        self.high_score = 0

    def reset_stats(self):
        """Initialize statistics that change during the game."""

        #Number lives.
        self.lives_left = self.ai_settings.lives_limit

        #Score.
        self.score = 0

        #Level.
        self.level = 0
